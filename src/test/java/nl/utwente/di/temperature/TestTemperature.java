package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestTemperature {
    @Test
    public void testTemperature1() throws Exception {
        TemperatureConverter temperatureConverter = new TemperatureConverter();
        double temperature = temperatureConverter.celsiusToFarenheit(10.0);
        Assertions.assertEquals(50.0, temperature, 0.0, "Price of book 1");
    }
}
